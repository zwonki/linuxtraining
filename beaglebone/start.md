# BeagleBone Quick Start


## Documentation 

https://docs.beagleboard.org/latest/

https://elinux.org/Beagleboard:BeagleBoneBlack

https://git.beagleboard.org/beagleboard/linux

https://github.com/beagleboard/linux/tree/4.14

## Kernel serial interface

![](BBB_connections_croped.jpg)
```
picocom /dev/ttyUSB0 -b 115200
```

## Network configuration

### Setting up the static address
Assigning the IP _192.168.13.1_ to the network interface _eth0_.
```
root@beaglebone:~# cat /etc/network/interfaces
...
# The primary network interface
auto eth0
iface eth0 inet static
address 192.168.13.1
netmask 255.255.255.0
...
```

### SSH
```
ssh debian@192.168.13.1
```
default password: temppwd

```commandline
$ cat ~/.ssh/config
Host BBB
    user debian
    HostName 192.168.13.1
```

## Kernel

Repository:
```commandline
git clone --depth 1 https://github.com/beagleboard/linux.git -b 4.14
```
Tools:
```commandline
sudo apt -y install git bc bison flex libssl-dev make libc6-dev libncurses5-dev lzop
wget https://releases.linaro.org/components/toolchain/binaries/7.5-2019.12/arm-linux-gnueabihf/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz
tar xf gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf.tar.xz
```
Config:
```commandline
cd linux
scp BBB:/proc/config.gz .
gzip -d config.gz
mv config .config
make menuconfig
```
or
```commandline
cp arch/arm/configs/bb.org_defconfig .config
```
Compilation:
```commandline
export CC=<ROOT_DIRECTORY>/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-

git apply selinux_use_kernel_linux_socket_h_definitions_for_PF_MAX.patch

make ARCH=arm CROSS_COMPILE=${CC} bb.org_defconfig
make ARCH=arm -j8 CROSS_COMPILE=${CC} zImage
make ARCH=arm -j8 CROSS_COMPILE=${CC} modules
make ARCH=arm -j8 CROSS_COMPILE=${CC} dtbs
```

### Preparing the sdcard

uEnv.txt
```commandline
console=ttyS0,115200n8
sdargs=setenv bootargs console=ttyO0,115200n8 root=/dev/mmcblk1p1 ro rootfstype=ext4 rootwait debug earlyprintk mem=512M
sdboot=echo Booting from microSD ...; setenv autoload no ; load mmc 0:1 ${loadaddr} zImage ; load mmc 0:1 ${fdtaddr} am335x-boneblack.dtb ; run sdargs ; bootz ${loadaddr} - ${fdtaddr}
uenvcmd=run sdboot
```
copy the Kernel artifacts:
```commandline
cp arch/arm/boot/zImage /media/vboxuser/boot/
cp arch/arm/boot/dts/am335x-boneblack.dtb /media/vboxuser/boot/
```

### U-Boot

```commandline
git clone git://git.denx.de/u-boot.git --depth 1
export CROSS_COMPILE=<ROOT_DIRECTORY>/gcc-linaro-7.5.0-2019.12-x86_64_arm-linux-gnueabihf/bin/arm-linux-gnueabihf-
make am335x_evm_defconfig
make menuconfig
```

### ARM Trusted Firmware

```commandline
git clone https://git.beagleboard.org/beagleboard/arm-trusted-firmware.git

```