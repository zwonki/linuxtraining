# Linux Embedded Essentials exercises - part 1

1. Build the RT ready Linux Kernel:
    1. Match your Kernel with available latest patch https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/
    2. patch the Kernel
    3. Set Kernel to Real Time in menuconfig
    4. Compile it
    5. Download it to the board
