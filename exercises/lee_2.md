# Linux Embedded Essentials exercises - part 2

1. Try to use the DS3232 driver as the DS3231 driver
    1. decompile DTB files
    2. add i2c device node
    3. compile DTS to DTB format
    4. check if the driver is compiled or is a module
    5. check if the drivers work
2. Build your custom character device driver which will store the string on write and returns it on read 
