# Inter Process Communication

1. Using shared memory and semaphore create two types of processes:
    1. Producer which will print every 1s text line starting with producer id: ProducerX, where X is PID of producer (function getpid()) and random text to SHM ; spawn two producers. 
    2. Consumer which will print data from SHM and wrapping it in ConsumerX(<SHM data>) format. Consumer shall write number of read lines. Use semaphore as synchronization mechanism. 
2. Run all of the producers and consumers as the children of the third type of process: Manager; use fork(), exec() and wait()
