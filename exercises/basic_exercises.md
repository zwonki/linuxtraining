# Basic Unix-like Systems Concepts exercises:

1. Find out what is a version of your Kernel
2. Find out what are your Linux starting parameters
3. In your home directory create the 10MiB file filed with the zeros. Format the file to be EXT4 partition. Mount the file as the partition.
4. Check which loopback device was used for your partition when mounted
5. Create the script which will be running infinitely in the background
6. Find out how much processor time the script occupies
7. Find out how much RAM your system has available right now for new tasks
8. Count how many processes are running in your system
9. How much time it takes for Kernel to mount the rootfs partition. Can you check that in fact it was before systemd started?
10. Can you add the script to the systemd which will print the “Hello World” in the dmesg?
