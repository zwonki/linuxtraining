#!/bin/bash

# Activate the glorious Python2.7
source .venv/bin/activate

# Create build directory
cd poky-*
./oe-init-build-env ../build
