#!/bin/bash

sudo apt -y install bc build-essential chrpath diffstat gawk git lz4
sudo apt -y install libncurses5-dev pkg-config socat subversion texi2html texinfo u-boot-tools

# All hail to glorious Python 2.7
sudo apt -y install python2 python2-pip-whl python2-setuptools-whl
test -f get-pip.py || wget https://bootstrap.pypa.io/pip/2.7/get-pip.py
python2 get-pip.py
python2.7 -m pip install virtualenv

bash -c "virtualenv --python=python2.7 .venv"
