#!/bin/bash

mkdir -p workspace
cd workspace

../prepare_env.sh
../download_sources.sh --depth 1
../prepare_build_dir.sh

