#!/bin/bash

BR_VER=buildroot-2021.08
BR_TGZ=$BR_VER.tar.gz

wget https://buildroot.org/downloads/$BR_TGZ

tar xf $BR_TGZ

cd $BR_VER

make raspberrypi4_defconfig

#make menuconfig
make nconfig
make busybox-menuconfig
make linux-menuconfig
