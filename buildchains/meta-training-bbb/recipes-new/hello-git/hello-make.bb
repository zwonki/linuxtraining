SUMMARY = "Hello with Makefile"

PRIORITY = "optional"

LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1ebbd3e34237af26da5dc08a4e440464"

SRC_URI = "git://github.com/wzwonarz/yocto-source-test.git;branch=main"
SRCREV = "${AUTOREV}" 

S = "${WORKDIR}/git"

EXTRA_OEMAKE = "'CC=${CC}' 'RANLIB=${RANLIB}' 'AR=${AR}' \
   'CFLAGS=${CFLAGS} -I${S}/. -DWITHOUT_XATTR' 'BUILDDIR=${S}'"

do_install () {
   oe_runmake install DESTDIR=${D} BINDIR=${bindir} SBINDIR=${sbindir} \
      MANDIR=${mandir} INCLUDEDIR=${includedir}
}

INSANE_SKIP_${PN} = "ldflags"
