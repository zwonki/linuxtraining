# Package summary
SUMMARY = "Hello World"
# License, for example MIT
LICENSE = "MIT"
# License checksum file is always required
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
 
# hello-world.c from local file
SRC_URI = "file://hello.c"
 
# Change source directory to workdirectory where hello-world.cpp is
S = "${WORKDIR}"

COMPATIBLE_MACHINE = "new-bbb"
 
# Compile hello-world from sources, no Makefile
do_compile() {
    ${CC} -Wall hello.c -o hello
}
 
# Install binary to final directory /usr/bin
do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${S}/hello ${D}${bindir}
}


INSANE_SKIP:${PN} = "ldflags"
