FILESEXTRAPATHS:append := "${THISDIR}/files"

COMPATIBLE_MACHINE = '(beaglebone-yocto|beaglebone-custom)'


SRC_URI += "file://custom_defconfig \
            file://am335x-boneblack-custom.dts;subdir=git/arch/${ARCH}/boot/dts \
            file://add_dts.patch"

KERNEL_DEVICETREE:append = " am335x-boneblack-custom.dtb "

