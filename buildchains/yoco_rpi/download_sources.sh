#!/bin/bash

mkdir yocto-build
cd yocto-build

# clone repos
git clone git://git.yoctoproject.org/poky.git poky
cd poky
git checkout febbe2944c0c4a04b85fa98fdc261186115954d8
cd ..
git clone -b dunfell git://git.openembedded.org/meta-openembedded
git clone -b dunfell git://git.yoctoproject.org/meta-raspberrypi

#prepare build config
mkdir -p build/conf

cp poky/meta-poky/conf/local.conf.sample build/conf/local.conf
cp poky/meta-poky/conf/bblayers.conf.sample build/conf/bblayers.conf

echo "Remember to update bblayers.conf paths, use absolute paths"
