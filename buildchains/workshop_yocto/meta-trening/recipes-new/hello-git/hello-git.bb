# Package summary
SUMMARY = "Hello World"
# License, for example MIT
LICENSE = "GPLv3"
# License checksum file is always required
LIC_FILES_CHKSUM = "file://LICENSE;md5=1ebbd3e34237af26da5dc08a4e440464"
 
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI = "git://github.com/wzwonarz/yocto-source-test.git;branch=main"
SRCREV = "${AUTOREV}" 

SRC_URI += "file://hello.patch"

S = "${WORKDIR}/git"
 
# Compile hello-world from sources, no Makefile
do_compile() {
    ${CC} -Wall hello.c -o hello
}
 
# Install binary to final directory /usr/bin
do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${S}/hello ${D}${bindir}/hello-git
}


INSANE_SKIP_${PN} = "ldflags"
