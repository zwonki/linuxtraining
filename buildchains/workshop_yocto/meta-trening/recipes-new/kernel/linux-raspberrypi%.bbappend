FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://custom_defconfig \
            file://custom.dts"

#KBUILD_DEFCONFIG_new-rpi ?= "bcm2709_defconfig"

KERNEL_DEVICETREE += "custom.dtb"

do_configure_prepend() {
        cp ${WORKDIR}/custom.dts ${S}/arch/${ARCH}/boot/dts
}


