#!/bin/bash

OUT_DIR=optee_os/out/arm/core

ln -fs $OUT_DIR out

# for future
# ln -fs ../bootloaders/uboot/u-boot/

cd build

make -f rpi3.mk toolchains

make -f rpi3.mk optee-os

cd ..

SCRIPT_PATH=$(dirname $(realpath $0))

CLIENT_PATH=$SCRIPT_PATH/optee_client
OPTEE_PATH=$SCRIPT_PATH/optee_os

CROSS=$SCRIPT_PATH/toolchains/aarch64/bin/aarch64-linux-gnu-

echo $CLIENT_PATH

cd optee_client
make HOST_CROSS_COMPILE=$CROSS TA_DEV_KIT_DIR=$OPTEE_OS/out/arm/export-ta_arm64 CROSS_COMPILE=$CROSS
cd ..

cd optee_test
make CROSS_COMPILE=$CROSS OPTEE_CLIENT_EXPORT=$CLIENT_PATH/out/export/usr TA_DEV_KIT_DIR=$OPTEE_PATH/out/arm/export-ta_arm64 xtest
make CROSS_COMPILE=$CROSS OPTEE_CLIENT_EXPORT=$CLIENT_PATH/out/export/usr TA_DEV_KIT_DIR=$OPTEE_PATH/out/arm/export-ta_arm64 ta
cd ..

