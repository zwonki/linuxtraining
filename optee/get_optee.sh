#!/bin/bash

git clone https://github.com/OP-TEE/optee_os.git
git clone https://github.com/OP-TEE/build.git
git clone https://github.com/OP-TEE/optee_client.git
git clone https://github.com/OP-TEE/optee_test.git
git clone https://github.com/linaro-swg/optee_examples.git
