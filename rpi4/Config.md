# Raspberry PI 4 Configuration

## Hardware
Connect the UART converter to the pins GPIO14, GPIO15 and ground.
The UART bound rate is 115200. Which is common for UNIX-like system consoles.

Connect the RTC to the I2C port. Pins GPIO2, GPIO3 also the 3v3 and GND shall be connected.

![pinoout](R-Pi-4-GPIO-Pinout-1-768x572.webp "RPI4 Pinout")

## System
Install OS Lite 32bit from [raspberry page](https://www.raspberrypi.com/software/)

> wget https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-lite.zip

> unzip 2021-10-30-raspios-bullseye-armhf-lite.zip

> dd if=2021-10-30-raspios-bullseye-armhf-lite.img of=/dev/sdX bs=4M

In config.txt add:

> enable_uart=1

line.

In boot partition:

> touch ssh

## Cross-compiler for application
Cross-compilers installation:

> sudo apt install crossbuild-essential-armhf

> sudo apt install crossbuild-essential-arm64

> export CC=arm-linux-gnueabihf-gcc 

## Static IP
In file:

> sudo vi /etc/dhcpcd.conf

Add following:

> interface eth0

> static ip_address=192.168.0.101/24

> static routers=192.168.0.1

> static domain_name_servers=192.168.0.1

## All apt install commands

> sudo apt install git bc bison flex libssl-dev make libc6-dev libncurses5-dev

> sudo apt install crossbuild-essential-armhf

> sudo apt install crossbuild-essential-arm64


## Basic build commands for 32bit kernel


> cd linux

> make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcm2711_defconfig

> make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- menuconfig

> KERNEL=kernel7l
> make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules dtbs

The output kernel image shall appear in the:

> linux/arch/arm/boot

directory.

Deploy the image to the board and change the config.txt file:

> kernel=zImage

Note: Remember to deploy themodules with the deploy_kernel.sh commands.
