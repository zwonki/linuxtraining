#!/bin/bash

sudo apt install git bc bison flex libssl-dev make libc6-dev libncurses5-dev
sudo apt install crossbuild-essential-armhf
sudo apt install crossbuild-essential-arm64

git clone --depth=1 https://github.com/raspberrypi/linux
