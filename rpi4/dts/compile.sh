#!/bin/bash

LINUX_PATH=../../../linux

cpp -nostdinc -I $LINUX_PATH/include -I $LINUX_PATH/arch  -undef -x assembler-with-cpp bcm2711-rpi-4-b.dts bcm2711-rpi-4-b.dts.preprocessed

$LINUX_PATH/scripts/dtc/dtc -I dts -O dtb -o $1 bcm2711-rpi-4-b.dts.preprocessed
